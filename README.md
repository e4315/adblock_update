# Ad-Block update script

adblock_update.sh is a script for downloading files with lists of hosts that are to be blocked by Dnsmasq. It downloads files removes some characters and rows and creates dnsmasq.adlist.conf file and finally restarts dnsmasq service. It is based on scripts found on Ubuquiti forum.


## Files

- adblock_update.sh - main script
- adblock_update.conf - configuration file
- adblock_black.lst - Black list. Contains lists urls with advertising hosts.
- adblock_white.lst - White list. Chosen lists of domains that I do not want to block. List in "sed" format.



## Installation

### Copy files to your server or router
Copy files to /config/user-data/ directory on EdgeRouter. On linux to directory you want. The adblock_update.sh should have execute permission. Make it f.e. with this command:

```sh
$ chmod 750 adblock_update.sh
```

### Updating periodicaly
On linux add to cron comand like this.
```sh
0 3 * * 6 /path/adblock_update.sh > /var/log/adblock_update.log
```
Copy files to the /path/ directory.

On EdgeRouter add this to configuration
```sh
configure
set system task-scheduler task adblock_update executable path "/config/user-data/adblock_update.sh >> /var/log/adblock_update.log"
set system task-scheduler task adblock_update crontab-spec "0 3 * * 6"
commit; save
exit
```
## Note
Adblock-update script doesn't check amount space it uses. Small routersa like EdgeRouter X have not so many free flash memory.

