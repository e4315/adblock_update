#!/bin/bash
#
# Ad blocking list builder for dnsmasq by Boczek
#
# Main script
#
###############################################################################

config_file="./adblock_update.conf"
. ${config_file}

###############################################################################
TEMP_AD_FILE="${TEMP_DIR}dnsmasq.adlist.tmp"
# List of urls with sites that contains domains to block
BLACK_LIST_FILE="${WORK_DIR}adblock_black.lst"
WHITE_LIST_FILE="${WORK_DIR}adblock_white.lst"
# Dnsmasq configuration file
AD_FILE="/etc/dnsmasq.d/dnsmasq.adlist.conf"



function LOG () {
    DT=`date '+%Y-%m-%d %H:%M:%S'`
    echo "${DT}  $1"
}

function DETECT_DATA_FORMAT () {
	# check format 1
	# 127.0.0.1 domain.com
	#
	cnt=`head -50 $1 | grep ^127.0.0.1$'\(\t\| \)' | wc -l`
	if [ $cnt -gt 0 ]; then
		if [ $DEBUG -eq 1 ]; then LOG "Format 1"; fi
		data_fmt=1
	else
		# check format 2
		# 0.0.0.0 domain.com
		#
		cnt=`head -50 $1 | grep ^0.0.0.0$'\(\t\| \)' | wc -l`
		if [ $cnt -gt 0 ]; then
			if [ $DEBUG -eq 1 ]; then LOG "Format 2"; fi
			data_fmt=2
		else

			# check format 3
			# <IP> domain.com
			#
			cnt=`head -50 $1 | grep -Eo '^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\s' | wc -l`
			if [ $cnt -gt 0 ]; then
				if [ $DEBUG -eq 1 ]; then LOG "Format 3"; fi
				data_fmt=3
			else
				# check format 4
				# 0.0.0.0domain.com
				#
				cnt=`head -50 $1 | grep '^0.0.0.0[a-z]' | wc -l`
				if [ $cnt -gt 0 ]; then
					if [ $DEBUG -eq 1 ]; then LOG "Format 4"; fi
					data_fmt=4
				else
					if [ $DEBUG -eq 1 ]; then LOG "Unknown data format - assuming domain list"; fi
					data_fmt=100
				fi
			fi
		fi
	fi
}


LOG "=== AdBlock - begin ==="

if [ $TESTING -eq 0 ]; then
    LOG "Compressing old ad file"
    gzip -v -9 $AD_FILE
fi

if [ -f ${TEMP_AD_FILE} ]; then
    LOG "Removing old temp file"
    rm -f ${TEMP_AD_FILE}
fi

if [ ! -d ${TEMP_DIR} ]; then
    LOG "Temporary directory doesn't exist. Creating..."
    mkdir ${TEMP_DIR}
fi


row_no=1
data_fmt=0

while read url; do
	LOG "Processing url no. ${row_no} - ${url}"
	#$data_fmt=0

	tmp_downloaded=${TEMP_DIR}${row_no}_downloaded.tmp
	tmp_file=${TEMP_DIR}${row_no}_file.tmp
	txt_hosts=${TEMP_DIR}${row_no}_hosts.txt

	if [ $TESTING -eq 0 ]; then
		curl -s "${url}" > "${tmp_downloaded}"
	else
		if [ ! -f "${tmp_downloaded}" ]; then
			curl -s "${url}" > "${tmp_downloaded}"
		fi
	fi

	if [ -f "${tmp_downloaded}" ]; then
		LOG "File size: `du -k ${tmp_downloaded} | awk '{print $1}'` KB"
		LOG "File rows: `cat "${tmp_downloaded}" | wc -l`"

		if [ `stat -c %s "${tmp_downloaded}"` -eq 0 ]; then
			LOG "Downloaded The file is 0 bytes long. Trying to log reason..."
			curl -v -s -S "${url}"
			continue
		fi

		# some data cleanup
			# remove CR
			# remove comment lines
			# remove empty rows
			# remove inline comments
			# remove lines starting with character [
			# remove html
		cat "${tmp_downloaded}" | \
			sed 's/\r$//' | \
			grep -v "^#" | \
			grep -v -e '^$' | \
			cut -d "#" -f 1 | \
			grep -v "^\[" | \
			sed -e 's/<[^>]*>//g' > "${tmp_file}"

		if [ $DEBUG -eq 0 ]; then rm -f ${tmp_downloaded}; fi

		DETECT_DATA_FORMAT "${tmp_file}"

		case $data_fmt in
		
			1)
				cat "${tmp_file}" | grep -w ^127.0.0.1 | sed 's/^127.0.0.1\s\{1,\}//' | grep -v -e '^localhost$' >> "${txt_hosts}"
				if [ $DEBUG -eq 1 ]; then head -n 10 "${txt_hosts}"; fi
				;;
			2)
				cat "${tmp_file}" | grep -w ^0.0.0.0 | cut -c 9- | sed 's/\s\{1,\}.*//' >> "${txt_hosts}"
				if [ $DEBUG -eq 1 ]; then head -n 10 "${txt_hosts}"; fi
				;;
			3)
				cat "${tmp_file}" | cut -d " " -f 2 >> "${txt_hosts}"
				if [ $DEBUG -eq 1 ]; then head -n 10 "${txt_hosts}"; fi
				;;
			4)
				cat "${tmp_file}" | grep ^0.0.0.0 | cut -c 8- >> "${txt_hosts}"
				if [ $DEBUG -eq 1 ]; then head -n 10 "${txt_hosts}"; fi
				;;
			*)
				cp "${tmp_file}" "${txt_hosts}"
				if [ $DEBUG -eq 1 ]; then head -n 10 "${txt_hosts}"; fi
				;;
		esac

		# sort new hosts and remove duplicate lines from it
		sort -o "${txt_hosts}" -u "${txt_hosts}"
		LOG "File contains: `cat "${txt_hosts}" | wc -l` hosts to be blocked"

		cat "${txt_hosts}" >> "${TEMP_AD_FILE}"
	else
		LOG "File does not exist"
	fi
	
	if [ $DEBUG -eq 0 ]; then 
		rm -f ${tmp_file}
		rm -f ${txt_hosts}
	fi
	
	LOG "---"
	((row_no=row_no+1))

done < $BLACK_LIST_FILE


if [ -f "$TEMP_AD_FILE" ]; then

	LOG "Building summary file"

	# remove the carriage return at the end of each line of the temporary file, and convert it into a Dnsmasq format
	sed -i -e 's/\r$//; s:.*:address=/&/0\.0\.0\.0:' $TEMP_AD_FILE

	# sort ad blocking list in the temp file and remove duplicate lines from it
	sort -o $TEMP_AD_FILE -t '/' -uk2 $TEMP_AD_FILE

	LOG "-------------------------------------------------"
	LOG "Domains to block: `cat $TEMP_AD_FILE | wc -l`"
	LOG "-------------------------------------------------"

	# uncomment the line below, and modify it to remove your favorite sites from the ad blocking list
	#sed -i -e '/www\.favoritesite\.com/d' $TEMP_AD_FILE

	LOG "Removing hosts fom white list"
	LOG "-------------------------------------------------"
	while read url; do
	    echo "Domain: $url"
	    sed -i -e "$url" $TEMP_AD_FILE
	done < $WHITE_LIST_FILE

	LOG "-------------------------------------------------"
	LOG "Final blocked domains: `cat $TEMP_AD_FILE | wc -l`"
	LOG "-------------------------------------------------"

	if [ $TESTING -eq 0 ]; then
		# remove old ad file
		rm -f $AD_FILE.gz
		# remove comments and unwanted html
		cat $TEMP_AD_FILE | grep "address=" > $AD_FILE
		rm -f $TEMP_AD_FILE

		ls -la /etc/dnsmasq.d/
		/etc/init.d/dnsmasq force-reload
	fi
else
	LOG "Error building the ad list, please try again."
	exit
fi


LOG "=== AdBlock - end ==="
